console.log("JS DOM")

// [Section]: Document Object Model(DOM)
	// allows us to access or modify the properties of an html element in a webpage
	// it is standards on how to get, change, add or delete html elements
	// we will be focusing only witdom in terms of managing forms
	
	// For selecting HTML elements we will be using document.querySelector / getElementById
		// Syntax: document.querySelector("html element")
		
		// CSS selectors
			// class selectors (.)
			// id selector (#)
			// tag selector (html tags)
			// universal selector (*)
			// atrribute selector ([attribute])
	
	// querySelectorAll
	let universalSelector = document.querySelectorAll("*")
	console.log(universalSelector)

	// querySelector
	let singleUniversalSelector = document.querySelector("*")
	console.log(singleUniversalSelector)

	let classSelector = document.querySelectorAll(".full-name")
	console.log(classSelector)

	let singleClassSelector = document.querySelector(".full-name")
	console.log(singleClassSelector)

	let singleIdSelector = document.querySelector("#txt-first-name")
	console.log(singleIdSelector)

	let tagSelector = document.querySelectorAll("input")
	console.log(tagSelector)

	let spanSelector = document.querySelector("span[id]")
	console.log(spanSelector)

	//getElement
		let element = document.getElementById("fullName")
		console.log(element)
		element = document.getElementsByClassName("full-name")
		console.log(element)

// [Section] Event Listeners
	// whenever a user interacts with a webpage, this action is considered as an event
	// working with events is a large part of creating interactivity in a webpage
	// specific function that will be triggered if the event happen.

	// The function us is "addEventListener", it takes two arguments
		// first argument, a string identifying the event
		// second argument, function that the listener will trigger once the "specified event" occur

		let txtFirstName = document.querySelector("#txt-first-name")

		// Add event listener
		// innerHTML for span and value for input
		txtFirstName.addEventListener("keyup", ()=>{
			console.log(txtFirstName.value)
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}` 
		})

		let txtLastName = document.querySelector("#txt-last-name")
		txtLastName.addEventListener("keyup", () => {
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})

		let colored = document.querySelector("#text-color")

		colored.addEventListener("change", () => {
			document.querySelector("#fullName").style.color = colored.value;
		})

